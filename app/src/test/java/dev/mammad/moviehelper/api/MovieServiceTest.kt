package dev.mammad.moviehelper.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dev.mammad.moviehelper.BuildConfig
import dev.mammad.moviehelper.ui.movieList.data.SortOrder
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.hamcrest.CoreMatchers.startsWith
import org.hamcrest.core.Is.`is`
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit

@RunWith(JUnit4::class)
class MovieServiceTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: MovieService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
            .build()
            .create(MovieService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun requestMovies() {
        runBlocking {
            enqueueResponse("movies.json")
            val resultResponse = getResultResponse()
            val request = mockWebServer.takeRequest()
            assertNotNull(resultResponse)
            assertThat(request.path, startsWith("/discover/movie"))
        }
    }

    @Test
    fun getMoviesResponse() {
        runBlocking {
            enqueueResponse("movies.json")
            val resultResponse = getResultResponse()
            assertNotNull(resultResponse)
            assertThat(resultResponse?.page, `is`(1))
            assertThat(resultResponse?.totalPages, `is`(500))
            assertThat(resultResponse?.totalResults, `is`(10000))
            val movies = resultResponse!!.results
            assertThat(movies.size, `is`(20))
        }
    }

    @Test
    fun getMovieItem() {
        runBlocking {
            enqueueResponse("movie_714.json")
            val resultResponse = getSingleMovieResponse()
            assertNotNull(resultResponse)

            val movie = resultResponse!!
            assertThat(movie.adult, `is`(false))
            assertNotNull(movie.belongsToCollection)
            assertThat(movie.budget, `is`(110000000))
            assertThat(movie.id, `is`(714))
            assertThat(movie.title, `is`("Tomorrow Never dies"))
        }
    }

    private suspend fun getResultResponse() = service.getMovies(
        apiKey = BuildConfig.API_DEVELOPER_TOKEN,
        page = 1,
        sortBy = SortOrder.ASC.orderValue
    ).body()

    private suspend fun getSingleMovieResponse() = service.getMovie(
        apiKey = BuildConfig.API_DEVELOPER_TOKEN,
        id = 714
    ).body()


    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
            ?.getResourceAsStream("api-response/$fileName")
        val source = inputStream?.source()?.buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(
            mockResponse.setBody(
                source!!.readString(Charsets.UTF_8)
            )
        )
    }
}
