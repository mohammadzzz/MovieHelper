package dev.mammad.moviehelper.api

import dev.mammad.moviehelper.ui.movieDetail.data.MovieModel
import dev.mammad.moviehelper.ui.movieList.data.TmdbApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("discover/movie")
    suspend fun getMovies(
        @Query("include_video") video: Boolean = false,
        @Query("include_adult") adult: Boolean = false,
        @Query("language") lang: String = "en-US",
        @Query("page") page: Int,
        @Query("sort_by") sortBy: String,
        @Query("api_key") apiKey: String
    ): Response<TmdbApiResponse>

    @GET("movie/{id}")
    suspend fun getMovie(
        @Path("id") id: Int,
        @Query("api_key") apiKey: String,
        @Query("language") lang: String = "en-US"
    ): Response<MovieModel>

}
