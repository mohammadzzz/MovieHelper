package dev.mammad.moviehelper.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dev.mammad.moviehelper.data.Result.Status.ERROR
import dev.mammad.moviehelper.data.Result.Status.SUCCESS
import kotlinx.coroutines.Dispatchers

fun <T> resultLiveData(
    networkCall: suspend () -> Result<T>
): LiveData<Result<T>> = liveData(Dispatchers.IO) {
    emit(Result.loading())
    val response = networkCall.invoke()
    if (response.status == SUCCESS) {
        emit(Result.success(response.data!!))
    } else if (response.status == ERROR) {
        emit(Result.error(response.message!!))
    }
}
