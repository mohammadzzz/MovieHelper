package dev.mammad.moviehelper.ui.movieDetail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import dev.mammad.moviehelper.ui.movieList.data.MovieRepository

class MovieDetailViewModel @ViewModelInject constructor(
    repository: MovieRepository
) : ViewModel() {

    private val _movieId = MutableLiveData<Int>()

    fun setMovieId(id: Int) {
        _movieId.postValue(id)
    }

    val movieDetail = _movieId.switchMap {
        repository.observeMovie(it)
    }

}