package dev.mammad.moviehelper.ui.movieList.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import dev.mammad.moviehelper.ui.movieList.data.TmdbApiResponse.Movie
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class MoviePageDataSourceFactory @Inject constructor(
    private val dataSource: MovieRemoteDataSource,
//    private val dao: MovieDao,
    private val scope: CoroutineScope,
    private val sortOrder: SortOrder
) : DataSource.Factory<Int, Movie>() {

    private val liveData = MutableLiveData<MoviePageDataSource>()

    override fun create(): DataSource<Int, Movie> {
        val source = MoviePageDataSource(dataSource/*, dao*/, scope, sortOrder)
        liveData.postValue(source)
        return source
    }

    companion object {
        private const val PAGE_SIZE = 20

        fun pagedListConfig() = PagedList.Config.Builder()
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPageSize(PAGE_SIZE)
            .setEnablePlaceholders(true)
            .build()
    }

}