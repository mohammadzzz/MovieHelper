package dev.mammad.moviehelper.ui.movieList.data

enum class SortOrder(val orderValue: String) {
    DESC("release_date.desc"),
    ASC("release_date.asc")
}
