package dev.mammad.moviehelper.ui.movieList.data

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import dev.mammad.moviehelper.data.resultLiveData
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class MovieRepository @Inject constructor(
//    private val dao: MovieDao,
    private val movieRemoteDataSource: MovieRemoteDataSource
) {

    fun observePagedMovies(
        coroutineScope: CoroutineScope,
        sortOrder: SortOrder
    ) = observeRemotePagedMovies(coroutineScope, sortOrder)

    private fun observeRemotePagedMovies(ioCoroutineScope: CoroutineScope, sortOrder: SortOrder)
            : LiveData<PagedList<TmdbApiResponse.Movie>> {
        val dataSourceFactory = MoviePageDataSourceFactory(
            movieRemoteDataSource,
//            dao,
            ioCoroutineScope,
            sortOrder
        )
        return LivePagedListBuilder(
            dataSourceFactory,
            MoviePageDataSourceFactory.pagedListConfig()
        ).build()
    }

    fun observeMovie(id: Int) = resultLiveData(
        networkCall = { movieRemoteDataSource.fetchMovie(id) })

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: MovieRepository? = null

        fun getInstance(/*dao: MovieDao, */movieRemoteDataSource: MovieRemoteDataSource) =
            instance ?: synchronized(this) {
                instance
                    ?: MovieRepository(/*dao,*/ movieRemoteDataSource).also { instance = it }
            }
    }
}