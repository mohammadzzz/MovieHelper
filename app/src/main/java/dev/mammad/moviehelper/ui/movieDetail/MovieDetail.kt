package dev.mammad.moviehelper.ui.movieDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import dev.mammad.moviehelper.data.Result
import dev.mammad.moviehelper.databinding.MovieDetailFragmentBinding
import dev.mammad.moviehelper.util.hide
import dev.mammad.moviehelper.util.show

@AndroidEntryPoint
class MovieDetail : Fragment() {

    private val args: MovieDetailArgs by navArgs()

    private val viewModel: MovieDetailViewModel by viewModels()

    private lateinit var binding: MovieDetailFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MovieDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setMovieId(args.movieId)
        binding.movieDetailFragmentImageBack.setOnClickListener { activity?.onBackPressed() }

        viewModel.movieDetail.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.SUCCESS -> {
                    binding.movieModel = it.data
                    binding.progressBarLayout.hide()
                }
                Result.Status.ERROR -> {
                    binding.progressBarLayout.hide()
                    Toast.makeText(
                        requireContext(),
                        "The resource you requested could not be found.",
                        Toast.LENGTH_LONG
                    ).show()
                    activity?.onBackPressed()
                }
                Result.Status.LOADING -> {
                    binding.progressBarLayout.show()
                }
            }

        })
    }


}