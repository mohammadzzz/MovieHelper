package dev.mammad.moviehelper.ui.movieList.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import dev.mammad.moviehelper.databinding.MainFragmentBinding
import dev.mammad.moviehelper.ui.OnMovieClickListener


@AndroidEntryPoint
class MainFragment : Fragment(), OnMovieClickListener {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var binding: MainFragmentBinding

    private lateinit var movieAdapter: MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        subscribeUi(movieAdapter)
    }

    private fun setupView() {
        movieAdapter = MovieAdapter(this)

        binding.mainFragmentMovieList.apply {
            val gridLayoutManager =
                GridLayoutManager(
                    this@MainFragment.requireContext(), COLUMN_COUNT
                )
            setHasFixedSize(true)
            layoutManager = gridLayoutManager
            adapter = movieAdapter
            setEmptyView(binding.progressBarLayout)
        }

        binding.mainFragmentSortButton.setOnClickListener {
            viewModel.toggleSortOrder()
        }

    }

    private fun subscribeUi(movieAdapter: MovieAdapter) {
        viewModel.movies.observe(viewLifecycleOwner, Observer {
            movieAdapter.submitList(it)
        })
    }

    override fun onClicked(movieId: Int) {
        val action =
            MainFragmentDirections.actionMainFragmentToMovieDetail(movieId)
        findNavController().navigate(action)
    }

    companion object {
        const val COLUMN_COUNT = 2
    }
}