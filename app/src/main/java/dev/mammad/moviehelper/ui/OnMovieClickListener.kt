package dev.mammad.moviehelper.ui

interface OnMovieClickListener {
    fun onClicked(movieId: Int)
}