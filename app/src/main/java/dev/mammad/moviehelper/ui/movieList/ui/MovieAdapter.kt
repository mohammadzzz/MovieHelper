package dev.mammad.moviehelper.ui.movieList.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.mammad.moviehelper.databinding.MovieItemBinding
import dev.mammad.moviehelper.ui.OnMovieClickListener
import dev.mammad.moviehelper.ui.movieList.data.TmdbApiResponse.Movie

class MovieAdapter(private val movieClickListener: OnMovieClickListener) :
    PagedListAdapter<Movie, MovieAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = getItem(position)
        holder.apply {
            bind(movie!!)
            itemView.tag = movie
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            MovieItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ), movieClickListener
        )
    }

    class ViewHolder(
        private val binding: MovieItemBinding,
        private val movieClickListener: OnMovieClickListener
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Movie) {
            binding.apply {
                movie = item
                clickListener = movieClickListener
                executePendingBindings()
            }
        }
    }
}

private class DiffCallback : DiffUtil.ItemCallback<Movie>() {

    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}
