package dev.mammad.moviehelper.ui.movieList.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import dev.mammad.moviehelper.ui.movieList.data.MovieRepository
import dev.mammad.moviehelper.ui.movieList.data.SortOrder

class MainViewModel @ViewModelInject constructor(
    repository: MovieRepository
) : ViewModel() {

    private val _sortOrder = MutableLiveData<SortOrder>(SortOrder.ASC)

    fun toggleSortOrder() {
        when (_sortOrder.value) {
            SortOrder.DESC -> _sortOrder.postValue(SortOrder.ASC)
            SortOrder.ASC -> _sortOrder.postValue(SortOrder.DESC)
        }
    }

    val movies = _sortOrder.switchMap { repository.observePagedMovies(viewModelScope, it) }

}