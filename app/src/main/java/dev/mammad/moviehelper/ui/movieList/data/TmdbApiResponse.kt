package dev.mammad.moviehelper.ui.movieList.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TmdbApiResponse(
    @SerialName("page")
    val page: Int?,
    @SerialName("total_results")
    val totalResults: Int?,
    @SerialName("total_pages")
    val totalPages: Int?,
    @SerialName("results")
    val results: List<Movie>
) {
    @Serializable
//@Entity(tableName = "movie")
    data class Movie(
        @SerialName("adult")
        val adult: Boolean,
        @SerialName("backdrop_path")
        val backdropPath: String?,
        @SerialName("genre_ids")
        val genreIds: List<Int>,
        @SerialName("id")
        val id: Int,
        @SerialName("original_language")
        val originalLanguage: String,
        @SerialName("original_title")
        val originalTitle: String,
        @SerialName("overview")
        val overview: String,
        @SerialName("popularity")
        val popularity: Double,
        @SerialName("poster_path")
        val posterPath: String?,
        @SerialName("release_date")
        val releaseDate: String? = "",
        @SerialName("title")
        val title: String,
        @SerialName("video")
        val video: Boolean,
        @SerialName("vote_average")
        val voteAverage: Double,
        @SerialName("vote_count")
        val voteCount: Int
    ) {
        override fun toString(): String {
            return title
        }

        fun getValidPosterPath() = "https://image.tmdb.org/t/p/w500${this.posterPath}"
    }

}