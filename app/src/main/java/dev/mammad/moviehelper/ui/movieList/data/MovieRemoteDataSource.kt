package dev.mammad.moviehelper.ui.movieList.data

import dev.mammad.moviehelper.BuildConfig
import dev.mammad.moviehelper.api.BaseDataSource
import dev.mammad.moviehelper.api.MovieService
import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(private val service: MovieService) :
    BaseDataSource() {

    suspend fun fetchMovies(page: Int, sortOrder: SortOrder) =
        getResult {
            service.getMovies(
                page = page,
                apiKey = BuildConfig.API_DEVELOPER_TOKEN,
                sortBy = sortOrder.orderValue
            )
        }

    suspend fun fetchMovie(movieId: Int) =
        getResult { service.getMovie(id = movieId, apiKey = BuildConfig.API_DEVELOPER_TOKEN) }

}
