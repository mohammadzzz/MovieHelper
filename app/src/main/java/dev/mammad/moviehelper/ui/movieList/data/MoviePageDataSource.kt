package dev.mammad.moviehelper.ui.movieList.data

import androidx.paging.PageKeyedDataSource
import dev.mammad.moviehelper.data.Result.Status.ERROR
import dev.mammad.moviehelper.data.Result.Status.SUCCESS
import dev.mammad.moviehelper.ui.movieList.data.TmdbApiResponse.Movie
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class MoviePageDataSource @Inject constructor(
    private val dataSource: MovieRemoteDataSource,
//    private val dao: MovieDao,
    private val scope: CoroutineScope,
    private val sortOrder: SortOrder
) : PageKeyedDataSource<Int, Movie>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Movie>
    ) {
        fetchData(1) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        val page = params.key
        fetchData(page) {
            callback.onResult(it, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        val page = params.key
        fetchData(page) {
            callback.onResult(it, page - 1)
        }
    }

    private fun fetchData(page: Int, callback: (List<Movie>) -> Unit) {
        scope.launch(getJobErrorHandler()) {
            val response = dataSource.fetchMovies(page, sortOrder)
            if (response.status == SUCCESS) {
                val results = response.data!!.results
//                dao.insertAll(results)
                callback(results)
            } else if (response.status == ERROR) {
                postError(response.message!!)
            }
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        postError(e.message ?: e.toString())
    }

    private fun postError(message: String) {
        Timber.e("An error happened: $message")
    }

}