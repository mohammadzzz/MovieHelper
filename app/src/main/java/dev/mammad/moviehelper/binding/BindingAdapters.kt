package dev.mammad.moviehelper.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("app:imageUrl")
fun bindMoviePoster(view: ImageView, string: String?) {
    Glide.with(view).load(string).into(view)
}