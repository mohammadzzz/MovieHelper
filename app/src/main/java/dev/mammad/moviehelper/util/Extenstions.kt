package dev.mammad.moviehelper.util

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide


fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun ImageView.load(url: String) {
    Glide.with(this).load(url).into(this)
}

fun Int.formatHourMinutes(): String {
    val hours = this.div(60)
    val minutes = this % 60
    return String.format("%d: %02d: 00", hours, minutes)
}